Using a 555

Water Flow Sensor - YF-S401 


More detials to come as it is installed/tested


![](Render.png)


BOM

Ref	                Qnty Value		Footprint	Description
C101, C104,         2	1uF	        C_Disc_D4.7mm_W2.5mm_P5.00mm	Unpolarized capacitor
C102, C103,         2	1uF	        CP_Radial_D5.0mm_P2.50mm	Polarised capacitor
C105, 	            1	10uF	    CP_Radial_D5.0mm_P2.50mm	Polarised capacitor
D101, D102,         2	LED	        TerminalBlock_bornier-2_P5.08mm	LED generic
J101, 	            1	Conn_01x03	:TerminalBlock_bornier-3_P5.08mm	Generic connector, single row, 01x03, script generated (kicad-library-utils/schlib/autogen/connector/)
J102 -J105,      	4	Conn_01x01	:MountingHole_3.2mm_M3	Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/)
J106, 	            1	Conn_01x03	:JST_EH_B03B-EH-A_03x2.50mm_Straight	Generic connector, single row, 01x03, script generated (kicad-library-utils/schlib/autogen/connector/)
J107, 	            1	Switch		:TerminalBlock_bornier-2_P5.08mm	Generic connector, single row, 01x02, script generated (kicad-library-utils/schlib/autogen/connector/)
Q101, Q102, Q103, 	3	2N7000		:TO-92_Inline_Wide	200V Vds, N-Channel MOSFET, 2.6V Logic Level, TO-92
R102, R101, R108, 	3	10k		    :R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal	Resistor
R103, R105, R107, 	3	100k	    :R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal	Resistor
R104, R106, 	    2	1k		    :R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal	Resistor
U101, 	            1	LM555	    :DIP-8_W7.62mm_Socket_LongPads	Timer, DIP-8/SOIC-8/SSOP-8
U102, 	            1	PC817	    :DIP-4_W7.62mm_Socket_LongPads	DC Optocoupler, Vce 35V, CTR 50-300%, DIP4


Design here.

http://www.falstad.com/circuit/circuitjs.html?cct=$+1+0.000005+35.60246606707791+56+5+50%0A165+224+208+336+208+0+0%0Aw+176+176+288+176+0%0AR+288+176+288+112+0+0+40+8+0+0+0.5%0AO+352+272+416+272+0%0Ar+176+176+176+304+0+10000%0Aw+176+304+176+336+0%0Aw+176+336+224+336+0%0Aw+224+304+176+304+2%0As+64+16+64+112+0+1+true%0Aw+176+432+400+432+0%0Ac+400+432+400+480+0+0.000001+7.207850133855907%0Ag+496+416+496+464+0%0Aw+352+272+400+336+0%0Ar+400+336+400+432+0+100000%0Ag+640+288+640+352+0%0Ag+400+480+400+496+0%0AR+496+0+496+-48+0+0+40+8+0+0+0.5%0Ar+448+336+400+336+0+1000%0Af+448+336+496+336+0+1.5+0.02%0Ar+448+336+448+416+0+100000%0Aw+448+416+496+416+0%0Aw+496+384+496+352+0%0As+496+64+496+144+0+0+false%0Ar+496+0+496+64+0+2000%0Aw+496+144+496+208+1%0A162+448+160+448+256+1+2.1024259+1+0+0+0.01%0A178+528+272+608+272+0+1+0.0002+7.999839759503107e-8+0.05+1000000+0.002+1%0Aw+496+384+496+416+0%0Aw+144+336+176+336+0%0Aw+496+320+528+320+0%0Aw+528+304+496+304+0%0Aw+528+272+528+240+0%0Aw+528+240+640+240+0%0Aw+608+288+640+288+0%0Aw+640+240+720+240+0%0Aw+720+240+720+528+0%0Aw+720+528+80+528+0%0Aw+80+336+144+336+0%0Ar+80+336+80+528+0+10000%0Aw+496+208+496+304+0%0Aw+448+256+496+320+0%0Ar+448+160+288+176+0+1000%0As+176+336+176+432+0+0+false%0Ao+3+64+0+4102+0.0000762939453125+0.00009765625+0+1%0Ao+10+64+0+4099+10+0.00009765625+1+2+10+3%0A